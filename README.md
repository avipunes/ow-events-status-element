# ow-events-status-element

This is a custom element ([web components](https://developer.mozilla.org/en-US/docs/Web/Web_Components) - can be used every where regardless the framework) built with [Svelte](https://svelte.dev/) to indicate game events status.

Provided by [eGamer.gg](https://www.eGamer.gg)

## Usage

`gameid` - OW game id.

`polling` - fetch events status every X ms.

```html
<ow-events-status-element gameid="10906">
    <div slot="green">
        😎
    </div>
</ow-events-status-element>
```

```html
<!-- polling status every 10 minutes -->
<ow-events-status-element gameid="10906" polling="600000">
    <div slot="red">
        😕
    </div>
</ow-events-status-element>
```

### Available slots:

-   `loading` - loading events status from OW API
-   `unsupported` - game id is not support by OW
-   `green` - events are up and running
-   `yellow`- partial functionality, some game events may be unavailable.
-   `red` - game events are unavailable.
-   `error` - failed to load events status.

### Examples:

-   [React](https://codesandbox.io/s/hungry-goldberg-p3mff)
-   [Vue](https://codesandbox.io/s/bold-archimedes-lrpth)
-   [Angular](https://codesandbox.io/s/quirky-silence-x6985)
-   [Vanilla](https://codesandbox.io/s/admiring-ellis-sbqdo)

### TODO

-   validate props types
-   tests
-   give indication for every event - flag
